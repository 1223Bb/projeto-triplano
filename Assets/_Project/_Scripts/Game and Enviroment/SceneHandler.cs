using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private EventManagerSO eventManagerSO;

    public static SceneHandler instance;

    private List<AsyncOperation> scenesLoading = new List<AsyncOperation>();
    private List<AsyncOperation> scenesUnloading = new List<AsyncOperation>();

    private void Awake()
    {
        OnGameLaunch();
    }

    private void OnGameLaunch()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        LoadScene((int)SceneIndexes.TitleScreen);
    }


    public void LoadScene(int sceneToLoadIndex)
    {
        loadingScreen.SetActive(true);
        if (SceneManager.GetActiveScene().buildIndex != (int)SceneIndexes.LoadingScreen)
        {
            scenesUnloading.Add(SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene()));
            StartCoroutine(UnloadScene());
        }
        scenesLoading.Add(SceneManager.LoadSceneAsync(sceneToLoadIndex, LoadSceneMode.Additive));
        StartCoroutine(GetSceneLoadProgress(sceneToLoadIndex));
    }

    public IEnumerator UnloadScene()
    {
        for (int i = 0; i < scenesUnloading.Count; i++)
        {
            while (!scenesUnloading[i].isDone)
            {
                yield return null;
            }
        }
    }

    public IEnumerator GetSceneLoadProgress(int sceneToLoadIndex)
    {
        for(int i=0; i<scenesLoading.Count; i++)
        {
            while (!scenesLoading[i].isDone)
            {
                yield return null;
            }
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneToLoadIndex));
        eventManagerSO.OnSceneLoad();
        scenesLoading.Clear();
        loadingScreen.SetActive(false);
    }
}
