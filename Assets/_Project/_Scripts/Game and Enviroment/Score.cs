using System.Collections;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private EventManagerSO eventManagerSO;
    private int score;
    private int multiplier;
    private int defaultMultiplier;

    private void Start()
    {
        SubscribeEvents();
        Init();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onGrabbablePickUp += AddScore;
        eventManagerSO.onScoreGemAcquire += StartDoubleScore;
        eventManagerSO.onGameStart += Init;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onGrabbablePickUp -= AddScore;
        eventManagerSO.onScoreGemAcquire -= StartDoubleScore;
        eventManagerSO.onGameStart -= Init;
    }

    private void Init()
    {
        score = 0;
        multiplier = 1;
        defaultMultiplier = multiplier;
    }

    private void AddScore(Item item)
    {
        Debug.Log("Multiplier: " + multiplier);
        this.score += item.GetValue() * multiplier;
        if (multiplier != 0)
        {
            eventManagerSO.OnScoreChange(this.score);
        }
    }

    private void StartDoubleScore()
    {
        if(multiplier != defaultMultiplier)
        {
            multiplier = defaultMultiplier;
            StopCoroutine(DoubleScore());
        }
        StartCoroutine(DoubleScore());
    }

    private IEnumerator DoubleScore()
    {
        multiplier *= 2;
        yield return new WaitForSeconds(5);
        multiplier = defaultMultiplier;
    }

    private void ChangeMultiplier(int multiplierChange)
    {
        multiplier += multiplierChange;
    }

    public int GetScore()
    {
        return score;
    }
}
