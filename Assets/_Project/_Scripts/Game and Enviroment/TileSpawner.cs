using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] tiles;
    [SerializeField] private List<GameObject> activeGroundTilePool;
    [SerializeField] private List<GameObject> standbyGroundTilePool;
    [SerializeField] private GameObject player;
    [SerializeField] private int activeTiles=99;
    [SerializeField] private int maxActiveTiles;
    [SerializeField] private int maxPooledTiles=13;
    [SerializeField] private int spawnedTilesQuantity;
    [SerializeField] private float threshold;
    [SerializeField] private EventManagerSO eventManagerSO;

    private GameObject baseTile;
    private float tileLength = 70f;

    private void Start()
    {
        SubscribeEvents();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onSceneLoad += Init;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onSceneLoad -= Init;
    }

    private void Update()
    {
        TileCulling();
    }

    private void TileCulling()
    {
        if (player.transform.position.z > threshold)
        {
            if (activeGroundTilePool.Count > 0)
            {
                DeactivateObject(activeGroundTilePool, standbyGroundTilePool, 0);
                PullRandomFromPool(Vector3.forward * (tileLength * spawnedTilesQuantity), standbyGroundTilePool, activeGroundTilePool);
                threshold = ((spawnedTilesQuantity + 2) - maxActiveTiles) * tileLength;
            }
        }
    }

    private void Init()
    {
        spawnedTilesQuantity = 0;
        threshold = tileLength*2;
        EmptyActiveLists();
        PopulateEmptyTileList();
        if(activeTiles>maxActiveTiles)
        {
            activeTiles = maxActiveTiles;
        }
        for (int i = 0; i < activeTiles - 1; i++)
        {
            if (i == 0)
            {
                PullRandomFromPool(Vector3.forward * -tileLength, standbyGroundTilePool, activeGroundTilePool);
                PullFromPool(Vector3.zero, standbyGroundTilePool, activeGroundTilePool, 0);
                spawnedTilesQuantity--;
            }
            else
            {
                PullRandomFromPool(Vector3.forward *(tileLength*spawnedTilesQuantity), standbyGroundTilePool, activeGroundTilePool);
            }
        }
    }

    private void PullRandomFromPool(Vector3 position, List<GameObject> standByList, List<GameObject> activeList)
    {
        int randomIndex = Random.Range(0, standByList.Count - 1);
        standByList[randomIndex].transform.position = position;
        standByList[randomIndex].SetActive(true);
        activeList.Add(standByList[randomIndex]);

        foreach(Transform child in standByList[randomIndex].transform)
        {
            if(child.GetComponent<Item>() != null)
            {
                child.gameObject.SetActive(true);
            }
        }
        standByList.RemoveAt(randomIndex);
        spawnedTilesQuantity++;
    }

    private void PullFromPool(Vector3 position, List<GameObject> standByList, List<GameObject> activeList, int index)
    {
        standByList[index].transform.position = position;
        standByList[index].SetActive(true);
        activeList.Add(standByList[index]);
        foreach (Transform child in standByList[index].transform)
        {
            if (child.GetComponent<Item>() != null)
            {
                child.gameObject.SetActive(true);
            }
        }
        standByList.RemoveAt(index);
        spawnedTilesQuantity++;
    }


    private void PopulateEmptyTileList()
    {
        if(activeGroundTilePool.Count==0 && standbyGroundTilePool.Count == 0)
        {
            maxActiveTiles = 0;
            foreach (GameObject tile in tiles)
            {
                if (tile == tiles[0])
                {
                    baseTile = Instantiate(tile);
                    standbyGroundTilePool.Add(baseTile);
                    baseTile.SetActive(false);
                    maxActiveTiles++;
                }
                else
                {
                    int numberOfXTiles = Random.Range(2, 4);
                    for (int i = 0; i < numberOfXTiles; i++)
                    {
                        GameObject groundTile = Instantiate(tile);
                        standbyGroundTilePool.Add(groundTile);
                        groundTile.SetActive(false);
                        maxActiveTiles++;
                    }
                }
            }
        }
    }

    private void DeactivateObject(List<GameObject> activeList, List<GameObject> standbyList, int i)
    {
        standbyList.Add(activeList[i]);
        activeList[i].SetActive(false);
        activeList.RemoveAt(i);
    }

    private void EmptyActiveLists()
    {
        if (activeGroundTilePool.Count > 0)
        {
            for (int i = activeGroundTilePool.Count - 1; i > -1; i--)
            {
                standbyGroundTilePool.Add(activeGroundTilePool[i]);
                activeGroundTilePool[i].SetActive(false);
                activeGroundTilePool.RemoveAt(i);
            }
        }
    }
}
