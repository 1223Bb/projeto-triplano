using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FXPooling : MonoBehaviour
{
    [SerializeField] private GameObject itemSFXPrefab;
    [SerializeField] private GameObject itemVFXPrefab;
    [SerializeField] private int startingFX;
    [SerializeField] private EventManagerSO eventManagerSO;

    private GameObject instantiadedSFX;
    private GameObject instantiadedVFX;
    private List<GameObject> standbySFXInstances;
    private List<GameObject> activeSFXInstances;
    private List<GameObject> standbyVFXInstances;
    private List<GameObject> activeVFXInstances;

    private void Start()
    {
        SubscribeEvents();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onSceneLoad += Init;
        eventManagerSO.onGrabbablePickUp += PlayFX;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onSceneLoad -= Init;
        eventManagerSO.onGrabbablePickUp -= PlayFX;
    }

    private void Init()
    {
        standbySFXInstances = new List<GameObject>();
        activeSFXInstances = new List<GameObject>();
        standbyVFXInstances = new List<GameObject>();
        activeVFXInstances = new List<GameObject>();
        for (int i=0; i < startingFX; i++)
        {
            InstantiateNewSFX();
            InstantiateNewVFX();
        }
    }

    private GameObject InstantiateNewSFX()
    {
        instantiadedSFX = Instantiate(itemSFXPrefab);
        standbySFXInstances.Add(instantiadedSFX);
        instantiadedSFX.SetActive(false);
        return instantiadedSFX;
    }

    private GameObject InstantiateNewVFX()
    {
        instantiadedVFX = Instantiate(itemVFXPrefab);
        standbyVFXInstances.Add(instantiadedVFX);
        instantiadedVFX.SetActive(false);
        return instantiadedVFX;
    }

    private void PlayFX(Item item)
    {
        SpawnSFX(item.transform.position);
        SpawnVFX(item.transform.position);
    }

    private void SpawnSFX(Vector3 position)
    {
        GameObject objToSpwn;
        int objIndex;
        if (standbySFXInstances.Count > 0)
        {
            objIndex = Random.Range(0, standbySFXInstances.Count - 1);
        }
        else
        {
            InstantiateNewSFX();
            objIndex = 0;
        }
        objIndex = Random.Range(0, standbySFXInstances.Count - 1);
        objToSpwn = standbySFXInstances[objIndex];
        objToSpwn.transform.position = position;
        objToSpwn.SetActive(true);
        standbySFXInstances.Remove(objToSpwn);
        activeSFXInstances.Add(objToSpwn);
        StartCoroutine(DespawnAfterSecs(objToSpwn, 1));
    }

    private void SpawnVFX(Vector3 position)
    {
        GameObject objToSpwn;
        int objIndex;
        if(standbyVFXInstances.Count > 0)
        {
            objIndex = Random.Range(0, standbyVFXInstances.Count - 1);
        }
        else
        {
            InstantiateNewVFX();
            objIndex = 0;
        }
        objToSpwn = standbyVFXInstances[objIndex];
        objToSpwn.transform.position = position;
        objToSpwn.SetActive(true);
        standbySFXInstances.Remove(objToSpwn);
        activeSFXInstances.Add(objToSpwn);
        StartCoroutine(DespawnAfterSecs(objToSpwn, 4));
    }

    private IEnumerator DespawnAfterSecs(GameObject go, int seconds)
    {
        yield return new WaitForSeconds(seconds);
        activeSFXInstances.Remove(go);
        go.SetActive(false);
        standbySFXInstances.Add(go);
    }
}
