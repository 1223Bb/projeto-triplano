using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : Item
{
    public override void Effect(int value)
    {
        eventManagerSO.OnShieldAcquire();
    }
}
