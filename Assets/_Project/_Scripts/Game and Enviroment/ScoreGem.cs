using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreGem : Item
{
    public override void Effect(int value)
    {
        eventManagerSO.OnScoreGemAcquire();
    }
}
