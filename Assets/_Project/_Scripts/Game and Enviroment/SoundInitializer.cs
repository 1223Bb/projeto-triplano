using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class SoundInitializer : MonoBehaviour
{
    [SerializeField] private AudioMixer mainAudioMixer;

    private void Start()
    {
        if(PlayerPrefs.HasKey(nameof(MixerVars.MusicVol)))
        {
            float treatedValue = Mathf.Log10(PlayerPrefs.GetFloat(nameof(MixerVars.MusicVol))) * 20;
            mainAudioMixer.SetFloat(MixerVars.MusicVol.ToString(), treatedValue);
        }
        else
        {
            mainAudioMixer.SetFloat(nameof(MixerVars.MusicVol), Mathf.Log10(0.5f) * 20);
            PlayerPrefs.SetFloat(nameof(MixerVars.MusicVol), 0.5f);
        }
        if (PlayerPrefs.HasKey(nameof(MixerVars.SFXVol)))
        {
            float treatedValue = Mathf.Log10(PlayerPrefs.GetFloat(nameof(MixerVars.SFXVol))) * 20;
            mainAudioMixer.SetFloat(nameof(MixerVars.SFXVol), treatedValue);
        }
        else
        {
            mainAudioMixer.SetFloat(nameof(MixerVars.SFXVol), Mathf.Log10(1f) * 20);
            PlayerPrefs.SetFloat(nameof(MixerVars.SFXVol), 1);
        }
    }
}
