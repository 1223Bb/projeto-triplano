using UnityEngine;

public class GamePause : MonoBehaviour
{
    [SerializeField] private EventManagerSO eventManagerSO;
    [SerializeField] private GameObject pnlPause;
    [SerializeField] private float timeToPause;
    [SerializeField] private InputManager inputMan;

    private bool isGamePaused = false;
    private Vector3 originalPausePanelSize;

    private void Start()
    {
        SubscribeEvents();
        originalPausePanelSize = pnlPause.transform.localScale;
        pnlPause.transform.localScale = Vector3.zero;
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onBtnClick += OnPauseClick;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onBtnClick -= OnPauseClick;
    }

    private void OnPauseClick(ButtonMethods btn)
    {
        if(btn == ButtonMethods.PauseGame)
        {
            isGamePaused = !isGamePaused;
            if(isGamePaused)
            {
                inputMan.enabled = false;
                LeanTween.scale(pnlPause, originalPausePanelSize, timeToPause)
                    .setIgnoreTimeScale(true)
                    .setEaseOutElastic();
                LeanTween.value(gameObject, 1, 0, timeToPause)
                    .setOnUpdate(SetTimeScale)
                    .setIgnoreTimeScale(true);
            }
            else
            {
                inputMan.enabled = true;
                LeanTween.scale(pnlPause, Vector3.zero, timeToPause)
                    .setIgnoreTimeScale(true)
                    .setEaseOutExpo();
                LeanTween.value(gameObject, 0, 1, timeToPause)
                    .setOnUpdate(SetTimeScale)
                    .setIgnoreTimeScale(true);
            }
        }
    }

    private void SetTimeScale(float value)
    {
        Debug.Log("value: "+value);
        Time.timeScale = value;
    }   
}
