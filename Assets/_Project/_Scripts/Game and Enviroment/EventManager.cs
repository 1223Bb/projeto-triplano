using System;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static EventManager current;
    public event Action onGameStart;
    public event Action<Item> onGrabbablePickUp;
    public event Action<int> onScoreChange;
    public event Action<SwipeDirection> onPlayerMove;
    public event Action onSceneLoad;
    public event Action onGameEnd;
    public event Action onShieldAcquire;
    public event Action onScoreGemAcquire;


    //UI related events
    public event Action<ButtonMethods> onBtnClick;

    //Input related events
    public event Action onTouchStart;
    public event Action<SwipeDirection> onDragEvent;
    public event Action onTouchEnd;

    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void OnBtnClick(ButtonMethods btn)
    {
        onBtnClick?.Invoke(btn);
    }

    public void OnScoreGemAcquire()
    {
        onScoreGemAcquire?.Invoke();
    }

    public void OnShieldAcquire()
    {
        onShieldAcquire?.Invoke();
    }

    public void OnGrabbablePickUp(Item item)
    {
        onGrabbablePickUp?.Invoke(item);
    }

    public void OnScoreChange(int score)
    {
        onScoreChange?.Invoke(score);
    }

    public void OnGameStart()
    {
        onGameStart?.Invoke();
    }

    public void OnGameEnd()
    {
        onGameEnd?.Invoke();
    }

    public void OnSceneLoad()
    {
        onSceneLoad?.Invoke();
    }

    //Input Events

    public void OnTouchStart()
    {
        onTouchStart?.Invoke();
    }

    public void OnDragEvent(SwipeDirection swipeDirection)
    {
        onDragEvent?.Invoke(swipeDirection);
    }

    public void OnTouchEnd()
    {
        onTouchEnd?.Invoke();
    }

    public void OnPlayerMove(SwipeDirection swipeDirection)
    {
        onPlayerMove?.Invoke(swipeDirection);
    }
}
