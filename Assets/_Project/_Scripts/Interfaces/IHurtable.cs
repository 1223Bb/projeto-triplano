public interface IHurtable
{
    public void GetHurt();
}
