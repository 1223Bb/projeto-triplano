using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Character Settings", menuName = "Player Character Settings")]
public class PlayerCharacterSettings : ScriptableObject
{
    public AnimationCurve tweenCurve;
    public float jumpForce;
    public float downForce;
    public float timeToChangeLanes;
    public float moveSpeed;
    public float speedGainPerSecond;
    public float slideDurationInSeconds;
    public float slideCooldownInSeconds;
}
