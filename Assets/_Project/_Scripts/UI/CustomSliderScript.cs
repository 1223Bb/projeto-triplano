using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomSliderScript : MonoBehaviour
{
    [SerializeField] private SliderMethods sliderMethod;
    [SerializeField] private EventManagerSO eventManagerSO;
    private Slider slider;

    private void Awake()
    {
        RemoveListeners();
        LoadValue(sliderMethod);
    }

    private void RemoveListeners()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.RemoveAllListeners();
    }

    private void OnEnable()
    {
        AddCustomListenerToOnValueChange();
    }

    private void OnDisable()
    {
        RemoveCustomOnValueChange();
    }

    private void AddCustomListenerToOnValueChange()
    {
        slider.onValueChanged.AddListener(OnValueChange);
    }

    private void RemoveCustomOnValueChange()
    {
        slider.onValueChanged.RemoveListener(OnValueChange);
    }

    private void OnValueChange(float value)
    {
        eventManagerSO.OnSliderValueChange(sliderMethod, value);
    }

    private void LoadValue(SliderMethods sliderMethod)
    {
        switch(sliderMethod)
        {
            default:
                break;
            case SliderMethods.MusicSlider:
                if(PlayerPrefs.HasKey(nameof(MixerVars.MusicVol)))
                {
                    slider.value = PlayerPrefs.GetFloat(nameof(MixerVars.MusicVol));
                }
                else
                {
                    slider.value = 1;
                }
                break;
            case SliderMethods.SFXSlider:
                if (PlayerPrefs.HasKey(nameof(MixerVars.SFXVol)))
                {
                    slider.value = PlayerPrefs.GetFloat(nameof(MixerVars.SFXVol));
                }
                else
                {
                    slider.value = 1;
                }
                break;
        }
    }
}
