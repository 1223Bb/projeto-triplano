using UnityEngine;
using UnityEngine.UI;

public class CustomButtonScript : MonoBehaviour
{
    [SerializeField] private ButtonMethods buttonMethod;
    [SerializeField] private EventManagerSO eventManagerSO;
    private Button button;

    private void Awake()
    {
        RemoveListeners();
    }

    private void RemoveListeners()
    {
        button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
    }

    private void OnEnable()
    {
        AddCustomListenerToOnClick();
    }

    private void OnDisable()
    {
        RemoveCustomOnClickListener();
    }

    private void AddCustomListenerToOnClick()
    {
        button.onClick.AddListener(onClick);
    }

    private void RemoveCustomOnClickListener()
    {
        button.onClick.RemoveListener(onClick);
    }

    private void onClick()
    {
        eventManagerSO.OnBtnClick(buttonMethod);
    }
}
