using UnityEngine;

public class MenuUIHandler : MonoBehaviour
{
    [SerializeField] private EventManagerSO eventManagerSO;

    private void Start()
    {
        SubscribeEvents();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onBtnClick += OnBtnClick;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onBtnClick -= OnBtnClick;
    }


    private void OnBtnClick(ButtonMethods btn)
    {
        switch(btn)
        {
            default:
                break;
            case ButtonMethods.PlayGame:
                OnPlayButtonClick();
                break;
            case ButtonMethods.QuitGame:
                OnExitButtonClick();
                break;
        }
    }

    public void OnPlayButtonClick()
    {
        SceneHandler.instance.LoadScene((int)SceneIndexes.Game);
    }

    public void OnExitButtonClick()
    {
        Application.Quit();
    }
}
