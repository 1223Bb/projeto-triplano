using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class GameUIHandler : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private GameObject pnlLeaderboard;
    [SerializeField] private EventManagerSO eventManagerSO;
    [SerializeField] private AudioMixer mainAudioMixer;

    private Vector2 leaderboardStartingPosition;
    private Vector2 leaderboardActivePosition;

    private void Start()
    {
        SubscribeEvents();
        Init();
    }


    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onGameStart += Init;
        eventManagerSO.onScoreChange += UpdateScoreValue;
        eventManagerSO.onGameEnd += GameOver;
        eventManagerSO.onBtnClick += BtnClick;
        eventManagerSO.onSliderValueChange += SliderValueChanged;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onGameStart -= Init;
        eventManagerSO.onScoreChange -= UpdateScoreValue;
        eventManagerSO.onGameEnd -= GameOver;
        eventManagerSO.onBtnClick -= BtnClick;
        eventManagerSO.onSliderValueChange -= SliderValueChanged;
    }

    private void BtnClick(ButtonMethods btn)
    {
        switch(btn)
        {
            default:
                break;
            case ButtonMethods.RetryGame:
                OnRetryButtonClick();
                break;
            case ButtonMethods.LeaveGame:
                OnMenuButtonClick();
                break;
        }
    }

    public void SliderValueChanged(SliderMethods slider, float value)
    {
        float treatedValue = Mathf.Log10(value) * 20;
        switch (slider)
        {
            default:
                break;
            case SliderMethods.MusicSlider:
                mainAudioMixer.SetFloat(MixerVars.MusicVol.ToString(), treatedValue);
                PlayerPrefs.SetFloat(MixerVars.MusicVol.ToString(), value);
                break;
            case SliderMethods.SFXSlider:
                mainAudioMixer.SetFloat(MixerVars.SFXVol.ToString(), treatedValue);
                PlayerPrefs.SetFloat(MixerVars.SFXVol.ToString(), value);
                break;
        }
    }

    private void GameOver()
    {
        pnlLeaderboard.SetActive(true);
        LeanTween.moveX(pnlLeaderboard, Screen.width/2, 0.5f)
            .setEaseInExpo();
    }

    private void UpdateScoreValue(int score)
    {
        scoreText.SetText(score.ToString());
    }

    private void Init()
    {
        scoreText.SetText("0");
        leaderboardStartingPosition = pnlLeaderboard.transform.position;
    }

    public void OnRetryButtonClick()
    {
        if (SceneHandler.instance != null)
        {
            SceneHandler.instance.LoadScene((int)SceneIndexes.Game);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void OnMenuButtonClick()
    {
        if (SceneHandler.instance != null)
        {
            SceneHandler.instance.LoadScene((int)SceneIndexes.TitleScreen);
        }
        else
        {
            SceneManager.LoadScene((int)SceneIndexes.TitleScreen);
        }
    }
}
