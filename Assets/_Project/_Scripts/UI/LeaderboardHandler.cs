using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Random = UnityEngine.Random;

public class LeaderboardHandler : MonoBehaviour
{
    [SerializeField] private Transform entryTemplate;
    [SerializeField] private Transform entryContainer;
    [SerializeField] private Score score;
    [SerializeField] private GameObject nameInputGameObject;
    [SerializeField] private GameObject leaderboardGameObject;
    [SerializeField] private TMP_InputField nameInputField;
    [SerializeField] private EventManagerSO eventManagerSO;

    private List<Transform> leaderboardEntryTransformList;
    private Leaderboard leaderboard;

    private void Awake()
    {
        InitializeLeaderboard();
    }

    private void InitializeLeaderboard()
    {
        entryTemplate.gameObject.SetActive(false);
        LoadLeaderboard();
        Debug.Log("Leaderboard initialization. Leaderboard Count: " + leaderboard.GetLeaderboard().Count);
        SortLeaderboard();
        LimitLeaderboard();
    }

    private void ShowLeaderboard()
    {
        LoadLeaderboard();
        SortLeaderboard();
        LimitLeaderboard();
        PopulateLeaderboard();
        nameInputGameObject.SetActive(false);
        leaderboardGameObject.SetActive(true);
    }

    private void Start()
    {
        SubscribeEvents();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onGameEnd += GameOver;
        eventManagerSO.onBtnClick += BtnClick;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onGameEnd -= GameOver;
        eventManagerSO.onBtnClick -= BtnClick;
    }

    private void BtnClick(ButtonMethods btn)
    {
        if(btn == ButtonMethods.SubmitName)
        {
            NameSubmit();
        }
    }

    private void PopulateLeaderboard()
    {
        leaderboardEntryTransformList = new List<Transform>();
        foreach (LeaderboardEntry leaderboardEntry in leaderboard.GetLeaderboard())
        {
            CreateLeaderboardEntry(leaderboardEntry, entryContainer, leaderboardEntryTransformList);
        }
    }

    private void LimitLeaderboard()
    {
        for (int i = leaderboard.GetLeaderboard().Count - 1; i > 9; i--)
        {
            leaderboard.GetLeaderboard().RemoveAt(i);
        }
    }

    private void SortLeaderboard()
    {
        for (int i = 0; i < leaderboard.GetLeaderboard().Count; i++)
        {
            for (int j = i + 1; j < leaderboard.GetLeaderboard().Count; j++)
            {
                if (leaderboard.GetLeaderboard()[j].GetScore() > leaderboard.GetLeaderboard()[i].GetScore())
                {
                    LeaderboardEntry aux = leaderboard.GetLeaderboard()[i];
                    leaderboard.GetLeaderboard()[i] = leaderboard.GetLeaderboard()[j];
                    leaderboard.GetLeaderboard()[j] = aux;
                }
            }
        }
    }

    private void LoadLeaderboard()
    {
        string jsonString = PlayerPrefs.GetString("leaderboard");
        leaderboard = JsonUtility.FromJson<Leaderboard>(jsonString);
        if(leaderboard == null)
        {
            List<LeaderboardEntry> leaderboardEntries = new List<LeaderboardEntry>();
            leaderboard = new Leaderboard(leaderboardEntries);
        }
    }

    private void CreateLeaderboardEntry(LeaderboardEntry leaderboardEntry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 50f;
        int rank = transformList.Count + 1;
        int score = leaderboardEntry.GetScore();
        string name = leaderboardEntry.GetName();
        Transform entry = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entry.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entry.Find("txtPosition").GetComponent<TextMeshProUGUI>().SetText(rank.ToString());
        entry.Find("txtName").GetComponent<TextMeshProUGUI>().SetText(name.ToString());
        entry.Find("txtScore").GetComponent<TextMeshProUGUI>().SetText(score.ToString());
        entry.gameObject.SetActive(true);
        transformList.Add(entry);
    }

    private void AddLeaderboardEntry(int score, string name)
    {
        LeaderboardEntry leaderboardEntry = new LeaderboardEntry(score, name);
        leaderboard.AddToLeaderboard(leaderboardEntry);
        string json = JsonUtility.ToJson(leaderboard);
        PlayerPrefs.SetString("leaderboard", json);
        PlayerPrefs.Save();
    }

    private void GameOver()
    {
        bool needName = false;
        if(leaderboard.GetLeaderboard().Count < 10)
        {
            needName = true;
        }
        else
        {
            if (score.GetScore() > leaderboard.GetLeaderboard()[9].GetScore())
            {
                needName = true;
            }
        }
        if(needName)
        {
            nameInputGameObject.SetActive(true);
            leaderboardGameObject.SetActive(false);
        }
        else
        {
            ShowLeaderboard();
        }
    }

    public void NameSubmit()
    {
        AddLeaderboardEntry(score.GetScore(), nameInputField.text);
        ShowLeaderboard();
    }

    private class Leaderboard
    {
        [SerializeField]
        private List<LeaderboardEntry> leaderboard;

        public Leaderboard(List<LeaderboardEntry> leaderboardEntries)
        {
            leaderboard = leaderboardEntries;
        }

        public List<LeaderboardEntry> GetLeaderboard()
        {
            return leaderboard;
        }

        public void AddToLeaderboard(LeaderboardEntry entry)
        {
            leaderboard.Add(entry);
        }
    }

    [System.Serializable]
    private class LeaderboardEntry
    {
        [SerializeField]
        private int score;
        [SerializeField]
        private string name;

        public LeaderboardEntry(int score, string name)
        {
            this.score = score;
            this.name = name;
        }

        public int GetScore()
        {
            return score;
        }

        public string GetName()
        {
            return name;
        }
    }
}
