using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    [SerializeField] private EventManagerSO eventManagerSO;

    public static InputManager current;

    private InputPlayerControls inputControls;
    private float verticalSwipeSpeedThreshold = 7f;
    private float horizontalSwipeSpeedThreshold = 5f;
    private bool isTouching;

    private void Awake()
    {
        if(current == null)
        {
            current = this;
        }
        else
        {
            Destroy(this);
        }
        inputControls = new InputPlayerControls();
    }

    private void OnEnable()
    {
        inputControls.Enable();
        SubscribeEvents();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
        inputControls.Disable();
    }

    private void SubscribeEvents()
    {
        inputControls.TouchControls.PrimaryTouch.performed += OnTouchStart;
        inputControls.TouchControls.PrimaryDelta.performed += OnDrag;
        inputControls.TouchControls.PrimaryTouch.canceled += OnTouchFinish;
    }

    private void UnsubscribeEvents()
    {
        inputControls.TouchControls.PrimaryTouch.performed -= OnTouchStart;
        inputControls.TouchControls.PrimaryDelta.performed -= OnDrag;
        inputControls.TouchControls.PrimaryTouch.canceled -= OnTouchFinish;
    }

    private void OnTouchStart(InputAction.CallbackContext obj)
    {
        eventManagerSO.OnTouchStart();
        isTouching = true;
    }

    private void OnTouchFinish(InputAction.CallbackContext obj)
    {
        eventManagerSO.OnTouchEnd();
        isTouching = false;
    }

    private void OnDrag(InputAction.CallbackContext obj)
    {
        if(isTouching)
        {
            Vector2 dragDelta = obj.ReadValue<Vector2>();
            if (dragDelta.x >= horizontalSwipeSpeedThreshold)
            {

                eventManagerSO.OnDragEvent(SwipeDirection.Right);
            }
            if (dragDelta.x <= -horizontalSwipeSpeedThreshold)
            {
                eventManagerSO.OnDragEvent(SwipeDirection.Left);
            }
            if (dragDelta.y >= verticalSwipeSpeedThreshold)
            {
                eventManagerSO.OnDragEvent(SwipeDirection.Up);
            }
            if (dragDelta.y <= -verticalSwipeSpeedThreshold)
            {
                eventManagerSO.OnDragEvent(SwipeDirection.Down);
            }
        }
    }
}
