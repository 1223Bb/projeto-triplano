// GENERATED AUTOMATICALLY FROM 'Assets/_Project/_Scripts/Input/InputPlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputPlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputPlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputPlayerControls"",
    ""maps"": [
        {
            ""name"": ""Touch Controls"",
            ""id"": ""a6f95976-9efc-4a99-b683-acce8a59cd68"",
            ""actions"": [
                {
                    ""name"": ""PrimaryTouch"",
                    ""type"": ""Button"",
                    ""id"": ""18c3a409-c28e-417e-b9ea-7a7e69624cfc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PrimaryDelta"",
                    ""type"": ""Value"",
                    ""id"": ""65b7eafc-e74b-44d4-81d7-fb516956f0a0"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9adc00ec-fb3c-435b-9567-a67ee580c07e"",
                    ""path"": ""<Touchscreen>/primaryTouch/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryTouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""68e3a602-4ac1-4642-a69f-4feab3887652"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryTouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b00579e6-cfd7-4f71-be05-72434e353e6d"",
                    ""path"": ""<Touchscreen>/primaryTouch/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryDelta"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a03b24c-b5e5-4715-8157-ea874ebf7adf"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryDelta"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Touch Controls
        m_TouchControls = asset.FindActionMap("Touch Controls", throwIfNotFound: true);
        m_TouchControls_PrimaryTouch = m_TouchControls.FindAction("PrimaryTouch", throwIfNotFound: true);
        m_TouchControls_PrimaryDelta = m_TouchControls.FindAction("PrimaryDelta", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Touch Controls
    private readonly InputActionMap m_TouchControls;
    private ITouchControlsActions m_TouchControlsActionsCallbackInterface;
    private readonly InputAction m_TouchControls_PrimaryTouch;
    private readonly InputAction m_TouchControls_PrimaryDelta;
    public struct TouchControlsActions
    {
        private @InputPlayerControls m_Wrapper;
        public TouchControlsActions(@InputPlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @PrimaryTouch => m_Wrapper.m_TouchControls_PrimaryTouch;
        public InputAction @PrimaryDelta => m_Wrapper.m_TouchControls_PrimaryDelta;
        public InputActionMap Get() { return m_Wrapper.m_TouchControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(TouchControlsActions set) { return set.Get(); }
        public void SetCallbacks(ITouchControlsActions instance)
        {
            if (m_Wrapper.m_TouchControlsActionsCallbackInterface != null)
            {
                @PrimaryTouch.started -= m_Wrapper.m_TouchControlsActionsCallbackInterface.OnPrimaryTouch;
                @PrimaryTouch.performed -= m_Wrapper.m_TouchControlsActionsCallbackInterface.OnPrimaryTouch;
                @PrimaryTouch.canceled -= m_Wrapper.m_TouchControlsActionsCallbackInterface.OnPrimaryTouch;
                @PrimaryDelta.started -= m_Wrapper.m_TouchControlsActionsCallbackInterface.OnPrimaryDelta;
                @PrimaryDelta.performed -= m_Wrapper.m_TouchControlsActionsCallbackInterface.OnPrimaryDelta;
                @PrimaryDelta.canceled -= m_Wrapper.m_TouchControlsActionsCallbackInterface.OnPrimaryDelta;
            }
            m_Wrapper.m_TouchControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PrimaryTouch.started += instance.OnPrimaryTouch;
                @PrimaryTouch.performed += instance.OnPrimaryTouch;
                @PrimaryTouch.canceled += instance.OnPrimaryTouch;
                @PrimaryDelta.started += instance.OnPrimaryDelta;
                @PrimaryDelta.performed += instance.OnPrimaryDelta;
                @PrimaryDelta.canceled += instance.OnPrimaryDelta;
            }
        }
    }
    public TouchControlsActions @TouchControls => new TouchControlsActions(this);
    public interface ITouchControlsActions
    {
        void OnPrimaryTouch(InputAction.CallbackContext context);
        void OnPrimaryDelta(InputAction.CallbackContext context);
    }
}
