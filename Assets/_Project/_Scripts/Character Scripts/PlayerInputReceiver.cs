using UnityEngine;

public class PlayerInputReceiver : MonoBehaviour
{
    [SerializeField] private EventManagerSO eventManagerSO;
    private float swipeCooldown=0.5f;
    private float swipeReadyTime;
    private bool canMove;
    private bool hasSwiped;

    private void Start()
    {
        SubscribeEvents();
        swipeReadyTime = Time.time;
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }
    private void SubscribeEvents()
    {
        eventManagerSO.onTouchStart += TouchStart;
        eventManagerSO.onDragEvent += Drag;
        eventManagerSO.onTouchEnd += TouchEnd;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onDragEvent -= Drag;
        eventManagerSO.onTouchEnd -= TouchEnd;
        eventManagerSO.onTouchStart -= TouchStart;
    }

    private void Update()
    {
        canMoveCheck();
    }

    private void TouchStart()
    {
        hasSwiped = false;
    }

    private void Drag(SwipeDirection obj)
    {
        if (canMove)
        {
            eventManagerSO.OnPlayerMove(obj);
            swipeReadyTime = Time.time + swipeCooldown;
            canMove = false;
            hasSwiped = true;
        }
    }

    private void TouchEnd()
    {
        canMove = true;
        if(!hasSwiped)
        {
            //Maybe use an item or something.
        }
    }

    private void canMoveCheck()
    {
        if(!canMove && swipeReadyTime <= Time.time)
        {
            canMove = true;
        }
    }

}
