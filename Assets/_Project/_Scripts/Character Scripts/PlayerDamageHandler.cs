using System.Collections;
using UnityEngine;
using Cinemachine;

public class PlayerDamageHandler : MonoBehaviour, IHurtable
{
    [SerializeField] private GameObject shield;
    [SerializeField] private EventManagerSO eventManagerSO;
    [SerializeField] private AudioSource hitSFX;
    [SerializeField] private CinemachineImpulseSource cinemachineImpulse;

    private bool isShielded;
    private bool isInvincible;

    private void Start()
    {
        SubscribeEvents();
        Init();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onGameStart += Init;
        eventManagerSO.onShieldAcquire += GetShield;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onShieldAcquire -= GetShield;
        eventManagerSO.onGameStart -= Init;
    }

    private void Init()
    {
        isShielded = false;
        isInvincible = false;
    }

    public void GetHurt()
    {
        if (!isInvincible)
        {
            cinemachineImpulse.GenerateImpulse();
            hitSFX.Play();
            if (isShielded)
            {
                isShielded = false;
                LeanTween.scale(shield, Vector3.zero, 0.3f);
                StartCoroutine(InvincibilityFrames());
            }
            else
            {
                eventManagerSO.OnGameEnd();
            }
        }
    }

    private IEnumerator InvincibilityFrames()
    {
        isInvincible = true;
        yield return new WaitForSeconds(1f);
        isInvincible = false;
    }

    private void GetShield()
    {
        if (!isShielded)
        {
        isShielded = true;
        LeanTween.scale(shield, new Vector3(2.5f, 2.5f, 2.5f), 0.3f);
        }
    }

    private void ScreenShake()
    {
        cinemachineImpulse.GenerateImpulse();
    }
}
