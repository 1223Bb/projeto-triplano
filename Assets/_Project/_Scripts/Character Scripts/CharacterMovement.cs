using System.Collections;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private PlayerCharacterSettings playerCharacterSettings;
    [SerializeField] private Collider hitbox;
    [SerializeField] private LayerMask environmentLayerMask;
    [SerializeField] private Rigidbody rigidBody;
    [SerializeField] private EventManagerSO eventManagerSO;
    [SerializeField] private GameObject parentGO;
    [SerializeField] private Animator anim;

    private int[] lanes = new int[3]
    {
        -3,
        0,
        3
    };

    private int currentLane;
    private Vector3 startingPosition;
    private bool isSliding = false;
    private AnimationCurve tweenCurve;
    private float jumpForce;
    private float downForce;
    private float timeToChangeLanes;
    private float moveSpeed;
    private float speedGainPerSecond;
    private float slideDurationInSeconds;
    private float slideCooldownInSeconds;


    private const float GroundcheckGap = 0.1f;
    private const float GroundcheckSphereRadius = 0.55f;
    private const float GroundcheckMaxDistance = 0.85f;

    private void Awake()
    {
        startingPosition = transform.position;
    }

    private void Start()
    {
        SubscribeEvents();
        Init();
    }


    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onPlayerMove += MoveCharacter;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onPlayerMove -= MoveCharacter;
    }

    private void Init()
    {
        currentLane = 1;
        transform.position = startingPosition;
        tweenCurve = playerCharacterSettings.tweenCurve;
        jumpForce = playerCharacterSettings.jumpForce;
        downForce = playerCharacterSettings.downForce;
        timeToChangeLanes = playerCharacterSettings.timeToChangeLanes;
        moveSpeed = playerCharacterSettings.moveSpeed;
        speedGainPerSecond = playerCharacterSettings.speedGainPerSecond;
        slideDurationInSeconds = playerCharacterSettings.slideDurationInSeconds;
        slideCooldownInSeconds = playerCharacterSettings.slideCooldownInSeconds;
}

    private void Update()
    {
        IncreaseSpeed();
    }

    private void FixedUpdate()
    {
        MoveForward();
    }

    private void IncreaseSpeed()
    {
        moveSpeed += speedGainPerSecond * Time.deltaTime;
    }

    private void MoveForward()
    {
        parentGO.transform.position += Vector3.forward * moveSpeed * Time.deltaTime;
    }

    private void MoveCharacter(SwipeDirection direction)
    {
        switch (direction)
        {
            case SwipeDirection.Left:
                if (currentLane > 0)
                {
                    LeanTween.cancel(parentGO);
                    LeanTween.moveLocalX(parentGO, lanes[currentLane - 1], timeToChangeLanes)
                        .setEase(tweenCurve);
                    currentLane--;
                    isGrounded();
                }
                break;
            case SwipeDirection.Right:
                if (currentLane < lanes.Length - 1)
                {
                    LeanTween.cancel(parentGO);
                    LeanTween.moveLocalX(parentGO, lanes[currentLane + 1], timeToChangeLanes)
                        .setEase(tweenCurve);
                    currentLane++;
                    isGrounded();
                }
                break;
            case SwipeDirection.Up:
                if(isGrounded())
                {
                    rigidBody.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
                    if(isSliding)
                    {
                        isSliding = false;
                        anim.SetBool("isSliding", false);
                        LeanTween.scaleY(hitbox.transform.gameObject, 1, 0.1f);
                    }
                    anim.SetTrigger("Jump");
                }
                break;
            case SwipeDirection.Down:
                if (!isSliding)
                {
                    if (isGrounded())
                    {
                        StartCoroutine(Slide());
                    }
                    else
                    {
                        rigidBody.AddForce(Vector3.down * downForce, ForceMode.VelocityChange);
                    }
                }
                break;
            default:
                break;
        }
    }

    private bool isGrounded()
    {
        RaycastHit hit;
        Physics.SphereCast(hitbox.transform.position + (Vector3.up * GroundcheckGap),
            GroundcheckSphereRadius,
            Vector3.down,
            out hit,
            GroundcheckMaxDistance,
            environmentLayerMask);
        if(hit.collider != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator Slide()
    {
        isSliding = true;
        anim.SetBool("isSliding", true);
        LeanTween.scaleY(hitbox.transform.gameObject, 0.5f, 0.2f);
        yield return new WaitForSeconds(slideDurationInSeconds);
        LeanTween.scaleY(hitbox.transform.gameObject, 1, 0.2f);
        yield return new WaitForSeconds(slideCooldownInSeconds);
        isSliding = false;
        anim.SetBool("isSliding", false);
    }
}
