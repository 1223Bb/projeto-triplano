using UnityEngine;

public class PlayerState : MonoBehaviour
{
    [SerializeField] private EventManagerSO eventManagerSO;
    [SerializeField] private Animator animator;

    private CharacterMovement movement;

    private void Awake()
    {
        movement = GetComponent<CharacterMovement>();
    }

    private void Start()
    {
        SubscribeEvents();
    }

    private void OnDisable()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        eventManagerSO.onGameStart += GameStart;
        eventManagerSO.onGameEnd += GameOver;
    }

    private void UnsubscribeEvents()
    {
        eventManagerSO.onGameStart -= GameStart;
        eventManagerSO.onGameEnd -= GameOver;
    }

    private void GameOver()
    {
        movement.enabled = false;
        animator.SetTrigger("Die");
    }

    private void GameStart()
    {
        movement.enabled = true;
    }
}
