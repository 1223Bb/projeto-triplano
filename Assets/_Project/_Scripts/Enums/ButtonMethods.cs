public enum ButtonMethods
{
    PlayGame,
    QuitGame,
    RetryGame,
    LeaveGame,
    SubmitName,
    PauseGame
}