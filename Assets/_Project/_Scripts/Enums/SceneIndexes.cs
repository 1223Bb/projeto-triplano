public enum SceneIndexes
{
    LoadingScreen = 0,
    TitleScreen = 1,
    Game = 2
}