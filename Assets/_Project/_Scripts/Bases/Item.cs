using UnityEngine;

public abstract class Item : MonoBehaviour
{
    [SerializeField] protected EventManagerSO eventManagerSO;
    [SerializeField] protected int value;
    
    protected float rotateRate = 72;

    public virtual void Effect(int value)
    {
        Debug.Log("This item does nothing.");
    }

    private void OnTriggerEnter(Collider other)
    {
        IHurtable playerCheck = other.GetComponent<IHurtable>();
        if(playerCheck != null)
        {
            OnItemGet();
        }
    }

    private void OnEnable()
    {
        SetAnimation();
    }

    private void Update()
    {
        Rotate();
    }
    private void OnItemGet()
    {
        Debug.Log("OnTriggerEnter");
        Effect(value);
        eventManagerSO.OnGrabbablePickUp(this);
        LeanTween.scale(gameObject, Vector3.zero, 0.5f)
            .setOnComplete(DestroyMe);
    }

    private void SetAnimation()
    {
        gameObject.transform.localScale = new Vector3(1, 1, 1);
        LeanTween.cancel(gameObject);
        LeanTween.moveY(gameObject, 0.25f, 1f)
        .setEaseInOutSine()
        .setLoopPingPong();
    }

    private void Rotate()
    {
        transform.Rotate(0,rotateRate*Time.deltaTime,0);
    }

    private void DestroyMe()
    {
        gameObject.SetActive(false);
    }

    public int GetValue()
    {
        return value;
    }
}